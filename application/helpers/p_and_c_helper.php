<?php
if (!function_exists('p_and_c')) {
	function p_and_c($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('p_and_c','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('p_and_c_front')) {
	function p_and_c_front($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('p_and_c','id','asc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_p_and_c_title')) {
		function get_p_and_c_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('p_and_c', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('p_and_c_sub')) {
	function p_and_c_sub($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('p_and_c_sub','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_p_and_c_sub_title')) {
		function get_p_and_c_sub_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('p_and_c_sub', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('p_and_c_sub_sub')) {
	function p_and_c_sub_sub($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('p_and_c_sub_sub','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}

?>