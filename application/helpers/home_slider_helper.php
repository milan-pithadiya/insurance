<?php
	function home_slider($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('home_slider','position','asc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
?>