<?php
	function loan_page($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('loan_page','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
	if (!function_exists('get_loan_title')) {
		function get_loan_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('loan_page', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
?>