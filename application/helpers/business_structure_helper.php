<?php
if (!function_exists('business_structure')) {
	function business_structure($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('business_structure','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('business_structure_title')) {
		function business_structure_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('business_structure', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}