<?php
if (!function_exists('bonds')) {
	function bonds($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('bonds','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_bonds_title')) {
		function get_bonds_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('bonds', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('bonds_sub')) {
	function bonds_sub($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('bonds_sub','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_bonds_sub_title')) {
		function get_bonds_sub_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('bonds_sub', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('bonds_sub_sub')) {
	function bonds_sub_sub($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('bonds_sub_sub','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('bonds_front')) {
	function bonds_front($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('bonds','id','asc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}

?>