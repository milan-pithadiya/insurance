<?php
if (!function_exists('life_and_health')) {
	function life_and_health($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('life_and_health','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_life_and_health_title')) {
		function get_life_and_health_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('life_and_health', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('life_and_health_sub')) {
	function life_and_health_sub($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('life_and_health_sub','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_life_and_health_sub_title')) {
		function get_life_and_health_sub_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('life_and_health_sub', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('life_and_health_sub_sub')) {
	function life_and_health_sub_sub($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('life_and_health_sub_sub','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('life_and_health_front')) {
	function life_and_health_front($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('life_and_health','id','asc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}

?>