<?php
if (!function_exists('category')) {
	function category($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('category','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}

if (!function_exists('get_category_title')) {
	function get_category_title($id) {
	    $CI = & get_instance();
	    $conditions = array("where"=>array("id"=>$id));
	    $info = $CI->common_model->select_data('category', $conditions);
	    // echo "<pre>";print_r($info);exit;
	    if ($info['row_count'] > 0) {
	        return $info['data'][0]['title'];
	    } else {
	        return '';
	    }
	}
}

if (!function_exists('product')) {
	function product($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('product','id','desc',$where);
		if (isset($info) && $info !=null){
			return $info;
		}
		else{
			return array();
		}
	}
}

if (!function_exists('get_product_title')){
	function get_product_title($id) {
	    $CI = & get_instance();
	    $conditions = array("where"=>array("id"=>$id));
	    $info = $CI->common_model->select_data('product', $conditions);
	    // echo "<pre>";print_r($info);exit;
	    if ($info['row_count'] > 0) {
	        return $info['data'][0]['title'];
	    } else {
	        return '';
	    }
	}
}

if (!function_exists('category_front')) {
	function category_front($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('category','id','asc',$where);
		if (isset($info) && $info !=null){
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('product_front')) {
	function product_front($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('product','id','asc',$where);
		if (isset($info) && $info !=null){
			return $info;
		}
		else{
			return array();
		}
	}
}
?>