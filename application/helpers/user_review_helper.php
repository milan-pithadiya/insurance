<?php
if (!function_exists('user_review')) {
	function user_review($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('user_review','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('user_review_front')) {
	function user_review_front($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('user_review','id','asc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}

if (!function_exists('user_review_sub')) {
	function user_review_sub($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('user_review_sub','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_user_title')) {
		function get_user_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('user', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['agency_name'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('user_review_sub_sub')) {
	function user_review_sub_sub($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('user_review_sub_sub','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}

?>