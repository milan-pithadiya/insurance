<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>
<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>General Contact Form</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>General Contact Form</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <section class="contact-area ptb-100">
        <div class="container">
            <div class="contact-form2">
                <form id="form" action="" method="post">
                    <div class="row">
                        <?php $this->load->view('include/messages');?>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <input type="hidden" name="selected_item" value="<?=(isset($selected_item))?$selected_item:''?>">
                            <div class="form-group">
                                <label for="category">Category <span class="required">*</span></label>
                                <select class="form-control" id="category_id" name="category_id" >
                                <?php 
                                  $category = category_front();
                                  if (isset($category) && $category != null) {
                                      foreach ($category as $key => $value) { ?>
                                          <option value="<?=$value['id']?>" <?=(isset($details[0]['category_id']) && $value['id']==$details[0]['category_id'])?'selected':''?>>Category <?=$value['id']?></option>
                                          <?php        
                                      }
                                  }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group ">
                                <label for="product">Product <span class="required">*</span></label>
                                <select class="form-control" id="product_id" name="product_id" >
                                    <?php 
                                      $product = product_front();
                                      if (isset($product) && $product != null) {
                                          foreach ($product as $key => $value) { ?>
                                              <option value="<?=$value['id']?>" <?=(isset($details[0]['product_id']) && $value['id']==$details[0]['product_id'])?'selected':''?>>Product <?=$value['id']?></option>
                                              <?php        
                                          }
                                      }
                                    ?>
                                  
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group language-box">
                                <label class="language">Preferred Language <span class="required">*</span></label>
                                <?php 
                                  $language = language();
                                  // print_r($language);exit;
                                  foreach ($language as $key => $value) { ?>
                                    <label class="container-checkbox"><?=$value['title']?>
                                    <input type="checkbox" class="preferred_language" name="preferred_language[]" value="<?=$value['id']?>" > 
                                    <span class="checkmark"></span>
                                  <?php
                                  }
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="name">Name <span class="required">*</span></label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name*">
                                <?= form_error("name", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group pl-1">
                                <label for="email">Email <span class="required">*</span></label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email Address*">
                                <?= form_error("email", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="cell-phone-number">Mobile number <span class="required">*</span></label>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile number*">
                            <?= form_error("mobile_number", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="business_name">Business Name</label>
                                <input type="text" name="business_name" id="business_name" class="form-control" placeholder="Business Name">
                                <?= form_error("business_name", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="address">Address <span class="required">*</span></label>
                                <textarea type="text" name="address" id="address" class="form-control" placeholder="Address" rows="6" cols="30"></textarea>
                                <?= form_error("address", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group ">
                                <label for="business_address">Business Address</label>
                                <textarea name="business_address" class="form-control" id="business_address" cols="30" rows="6" placeholder="Business Address"></textarea>
                                <?= form_error("business_address", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group pl-1">
                                <label for="messege">Messege</label>
                                <textarea class="form-control" name="message" id="message" cols="30" rows="6" placeholder="Your Message *"></textarea>
                                <?= form_error("message", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="default-btn check">Send Message <span></span></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="bg-map"><img src="<?=base_url('assets/img/bg-map.png')?>" alt="image"></div>
    </section>
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
    <script>        

        var id = [];   
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                'name': {required: true}, 
                'category_id': {required: true}, 
                'product_id': {required: true}, 
                'email': {required: true,email:true}, 
                'mobile_number': {required: true,minlength:10}, 
                'address': {required: true},
                'address': {required: true},
                'preferred_language': {required: true},
            },
            messages: {
                'name': {required:"Please enter name"}, 
                'category_id': {required:"Please enter category"}, 
                'product_id': {required:"Please enter product"}, 
                'email': {required:"Please enter email",email:"Please enter valid email"}, 
                'mobile_number': {required:"Please enter mobile number",minlength:"Enter minimum 10 digits mobile number"}, 
                'address': {required:"Please enter last address"}, 
                'preferred_language': {required:"Please select language"}, 
            }
        }); 
        $(document).on('click','.preferred_language',function(e){
            $(".preferred_language:checked").each(function() { 
                  id.push($(this).val());
            });
            console.log(id);
        });
        var form = $( "#form" );
        form.validate();
        $(document).on('click','.check',function(e){
          if(!form.valid()){
            $("#form").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });  
    </script>
</body>
</html>