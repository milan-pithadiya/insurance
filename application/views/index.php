<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    
    <!-- Start Main Banner Area -->
    <?php $this->load->view('include/slider_section');?>
    <!-- End Main Banner Area -->

    <!-- Start Services Boxes Area -->
    <?php $this->load->view('include/service_box_section');?>
    <!-- End Services Boxes Area -->

    <!-- Start About Area -->
    <?php $this->load->view('include/about_section');?>
    <!-- End About Area -->
    
    <!-- Start Services Area -->
    <?php $this->load->view('include/service_section');?>
    <!-- End Services Area -->


    <!-- Start Partner Area -->
    <section class="partner-area">
        <div class="container">
            <div class="partner-title">
                <h2>Agencies</h2>
            </div>

            <div class="partner-slides owl-carousel owl-theme">
                <?php
                    // echo "<pre>";print_r($agency_details);exit;
                    if (isset($agency_details) && $agency_details !=null) {
                          foreach ($agency_details as $key => $value) { ?> 
                            <div class="single-partner-item">
                                <a href="#">
                                    <img src="<?= base_url(AGENCY_IMAGE.$value['image'])?>" alt="image">
                                </a>
                            </div>
                            <?php
                        }
                    }
                ?>

            </div>
        </div>
    </section>
    <!-- End Partner Area -->

    <!-- Start Why Choose Us Area -->
    <section class="why-choose-us-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="why-choose-us-slides owl-carousel owl-theme">
                        <div class="why-choose-us-image bg1">
                            <img src="<?= base_url()?>assets/img/why-choose-img1.jpg" alt="image">
                        </div>

                        <div class="why-choose-us-image bg2">
                            <img src="<?= base_url()?>assets/img/why-choose-img2.jpg" alt="image">
                        </div>

                        <div class="why-choose-us-image bg3">
                            <img src="<?= base_url()?>assets/img/why-choose-img3.jpg" alt="image">
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 col-md-12">
                    <div class="why-choose-us-content">
                        <div class="content">
                            <div class="title">
                               <!--  <span class="sub-title">Your Benefits</span> -->
                                <h2>How It Works</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="features-list">
                                <div class="title">
                                    <h5> <span>1</span> Tell us what you’re looking for</h5>
                                    <h5> <span>2</span> We find the best agents in your area</h5>
                                    <h5> <span>3</span> You connect with the one(s) you like</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Why Choose Us Area -->

    <!-- Start Quote Area -->
    <section class="quote-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="quote-content">
                        <h2>Get a free quote</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                        <div class="image">
                            <img src="<?= base_url()?>assets/img/img1.png" alt="image">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="tab quote-list-tab">
                        <!-- <ul class="tabs">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Business</a></li>
                            <li><a href="#">Health</a></li>
                            <li><a href="#">Car</a></li>
                            <li><a href="#">Life</a></li>
                        </ul> -->
    
                        <div class="tab_content">
                            <div class="tabs_item">
                                <p>Our experts will reply you with a quote very soon</p>
                                <form id="free_quote" action="" method="post">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Your Name">
                                        <span id="#name_error" class="error"></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="Your Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="mobile_number" id="mobile_number" placeholder="Your Phone">
                                    </div>
                                    <div class="form-group">
                                        <select name="property_used_for" id="property_used_for">
                                            <option value="">Property Used For</option>
                                            <option >Home Insurance</option>
                                            <option >Business Insurance</option>
                                            <option >Health Insurance</option>
                                            <option >Travel Insurance</option>
                                            <option >Car Insurance</option>
                                            <option >Life Insurance</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="default-btn">Get A Free Quote <span></span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Quote Area -->

    <!-- Start CTR Area -->
    <section class="ctr-area">
        <div class="container">
            <div class="ctr-content">
                <h2>Insurances for your child's future</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                <a href="#" class="default-btn">Get a Quote <i class="flaticon-right-chevron"></i><span></span></a>
            </div>

            <div class="ctr-image">
                <img src="<?= base_url()?>assets/img/ctr-img.jpg" alt="image">
            </div>

            <div class="shape">
                <img src="<?= base_url()?>assets/img/bg-dot3.png" alt="image">
            </div>
        </div>
    </section>
    <!-- End CTR Area -->

    <!-- Start Our Mission Area -->
    <section class="our-mission-area mtb-100">
        <div class="container-fluid p-0">
            <div class="row m-0">
                <div class="col-lg-3 col-md-6 p-0">
                    <div class="mission-image bg-1">
                        <img src="<?= base_url()?>assets/img/mission-img1.jpg" alt="image">
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 p-0">
                    <div class="mission-text">
                        <div class="icon">
                            <i class="flaticon-target"></i>
                        </div>

                        <h3><?=(isset($mission_details) && !empty($mission_details))?$mission_details[0]['title']:''?></h3>
                        <p>
                            <?php
                                if(isset($mission_details) && !empty($mission_details)){
                                  $content = strip_tags($mission_details[0]['description']);
                                  echo strlen($content) > 110 ? substr($content, 0, 110) . "..." : $content;
                                }
                            ?>
                        </p>

                        <a href="<?=base_url('mission-and-vision')?>" class="default-btn">Learn More <span></span></a>

                        <div class="shape"><img src="<?= base_url()?>assets/img/bg-dot2.png" alt="image"></div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 p-0">
                    <div class="mission-image bg-2">
                        <img src="<?= base_url()?>assets/img/mission-img2.jpg" alt="image">
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 p-0">
                    <div class="mission-text">
                        <div class="icon">
                            <i class="flaticon-medal"></i>
                        </div>

                        <h3><?=(isset($history_details) && !empty($history_details))?$history_details[0]['title']:''?></h3>
                        <p>
                            <?php
                                if(isset($history_details) && !empty($history_details)){
                                  $content = strip_tags($history_details[0]['description']);
                                  echo strlen($content) > 110 ? substr($content, 0, 110) . "..." : $content;
                                }
                            ?>
                        </p>

                        <a href="<?=base_url('mission-and-vision')?>" class="default-btn">Learn More <span></span></a>

                        <div class="shape"><img src="<?= base_url()?>assets/img/bg-dot2.png" alt="image"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Mission Area -->

    <!-- Start Our Achievements Area -->
    <section class="achievements-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="achievements-content">
                        <div class="title">
                            <span class="sub-title">Number</span>
                            <h2>Our Achievements</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-6 col-sm-4">
                                <div class="single-funfact">
                                    <i class="flaticon-flag"></i>
                                    <h3><span class="odometer" data-count="65">00</span></h3>
                                    <p>Countries</p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-6 col-sm-4">
                                <div class="single-funfact">
                                    <i class="flaticon-group"></i>
                                    <h3><span class="odometer" data-count="107">00</span> <span class="sign-icon">m</span></h3>
                                    <p>Clients</p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-12 col-sm-4">
                                <div class="single-funfact">
                                    <i class="flaticon-medal"></i>
                                    <h3><span class="odometer" data-count="150">00</span></h3>
                                    <p>Wining Awards</p>
                                </div>
                            </div>
                        </div>

                        <div class="bg-dot"><img src="<?= base_url()?>assets/img/bg-dot.png" alt="image"></div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="divider"></div>
                    <div class="achievements-image-slides owl-carousel owl-theme">
                        <div class="single-achievements-image bg1">
                            <img src="<?= base_url()?>assets/img/achievements-img1.jpg" alt="image">
                        </div>

                        <div class="single-achievements-image bg2">
                            <img src="<?= base_url()?>assets/img/achievements-img2.jpg" alt="image">
                        </div>

                        <div class="single-achievements-image bg3">
                            <img src="<?= base_url()?>assets/img/achievements-img3.jpg" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Achievements Area -->

    <!-- Start Join Area -->
    <section class="join-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12">
                    <div class="join-image text-center">
                        <img src="<?= base_url()?>assets/img/woman.png" alt="image">
                    </div>
                </div>

                <div class="col-lg-7 col-md-12">
                    <div class="join-content">
                        <h2>Great Place to Work</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>

                        <a href="<?=base_url('register')?>" class="default-btn">become A Agent  <i class="flaticon-right-chevron"></i><span></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Join Area -->

    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
</body>
</html>