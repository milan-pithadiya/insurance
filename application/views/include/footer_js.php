<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script src="<?= base_url()?>assets/js/popper.min.js"></script>
<script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/js/parallax.min.js"></script>
<script src="<?= base_url()?>assets/js/owl.carousel.min.js"></script>
<script src="<?= base_url()?>assets/js/slick.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.meanmenu.js"></script>
<script src="<?= base_url()?>assets/js/jquery.appear.min.js"></script>
<script src="<?= base_url()?>assets/js/odometer.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url()?>assets/js/wow.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.ajaxchimp.min.js"></script>
<script src="<?= base_url()?>assets/js/form-validator.min.js"></script>
<script src="<?= base_url()?>assets/js/contact-form-script.js"></script>
<script src="<?= base_url()?>assets/js/main.js"></script>
<script src="<?= base_url(); ?>assets/validation/jquery.validate.js"></script>
<script src="<?= base_url()?>assets/authority/js/toastr.min.js"></script>
<script src="<?= base_url()?>assets/js/chosen.jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(window).resize(function(){
            if ($(window).width() >= 980){
              $(".navbar .dropdown-toggle2").hover(function () {
                 $(this).parent().toggleClass("show");
                 $(this).parent().find(".dropdown-menu2").toggleClass("show"); 
               });
              $( ".navbar .dropdown-menu2" ).mouseleave(function() {
                $(this).removeClass("show");  
              });
            }   
        });
    });
    $(".chosen-select").chosen({
      no_results_text: "Oops, nothing found!"
    })
    window.onscroll = function() {myFunction()};

        var header = document.getElementById("myHeader");
        if(header){
          var sticky = header.offsetTop;
        }

        function myFunction() {
            if(header){
              if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
              } else {
                header.classList.remove("sticky");
              }
            }         
        }
        $(document).ready(function() {
          var agenturl = "<?php echo base_url("agent-listing") ?>";
          $(document).on('click','.clear_zip_code',function(){
            $.ajax({
              type: "POST",
              url: BASE_URL + 'agent_listing/clear',
              async: false,
              success: function (response) {
                var response = JSON.parse(response);
                if (response.success) {
                  window.location = agenturl;  
                }
              },
            });
          });

          $(document).on('click','.select_agency',function(){
            var agent_id = $(this).data('agent_id');
            $.ajax({
              type: "POST",
              data:{agent_id:agent_id},
              url: BASE_URL + 'agent_listing/select_agency',
              async: false,
              success: function (response) {
                var response = JSON.parse(response);
                if (response.result=="Success") {
                    
                    $(function() {
                        toastr.options = {
                        positionClass : "toast-bottom-right"
                        };
                        toastr.success(response.message);
                        setTimeout(function() {
                          window.location.reload(); 
                        }, 1000);
                    });
                 }else{
                    $(function() {
                        toastr.options = {
                        positionClass : "toast-bottom-right"
                        };
                        toastr.error(response.message);
                        setTimeout(function() {
                          window.location.reload(); 
                        }, 1000);
                    });
                }
              },
            });
          });

          $(document).on('submit','#free_quote',function(e){
            e.preventDefault();

            var name = $("#name").val();
            var email = $("#email").val();
            var mobile_number = $("#mobile_number").val();
            var property_used_for = $("#property_used_for").val();



            $.ajax({
              type: "POST",
              data:{name:name,email:email,mobile_number:mobile_number,property_used_for:property_used_for},
              url: BASE_URL + 'quote_form/free_quote',
              async: false,
              success: function (response) {

                var response = JSON.parse(response);
                
                console.log(response);
                if (response.result=="Success") {
                    
                    $(function() {
                        toastr.options = {
                        positionClass : "toast-bottom-right"
                        };
                        toastr.success(response.message);
                        setTimeout(function() {
                          window.location.reload(); 
                        }, 1000);
                    });
                 }else{
                    $(function() {
                        toastr.options = {
                        positionClass : "toast-bottom-right"
                        };
                        if(response.message.hasOwnProperty("name")){
                          toastr.error(response.message.name);
                          $("#name").focus();
                          return false;
                        }else if(response.message.hasOwnProperty("email")){
                          toastr.error(response.message.email);
                          $("#email").focus();
                          return false;
                        }else if(response.message.hasOwnProperty("mobile_number")){
                          toastr.error(response.message.mobile_number);
                          $("#mobile_number").focus();
                          return false;
                        }else if(response.message.hasOwnProperty("property_used_for")){
                          toastr.error(response.message.property_used_for);
                          $("#property_used_for").focus();
                          return false;
                        }
                    });
                }
              },
            });
          });
          
        });
        $(document).on('click','.general_quote_select',function(){
            var menu_name = $(this).data('menu_name');
            $.confirm({
              title: "Select form",
              content: false,
              buttons: {
                  specialKey: {
                      text: 'General Quote Form',
                      action: function(){
                          window.location=BASE_URL+'general-quote-form/'+menu_name;
                      }
                  },
                  alphabet: {
                      text: 'General Quote Form Auto',
                      action: function(){
                          window.location=BASE_URL+'general-quote-form-auto/'+menu_name;
                      }
                  }
              }
          })
        });
</script>