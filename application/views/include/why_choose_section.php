<!-- Facts -->
  <section id="facts" class="section-padding dark-overlay parallex-bg">
    <div class="container">
      <div class="section-header text-center">
        <h2>why choose deals of loan ?</h2>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class="facts-box z-index">
            <div class="icon-box"><i class="fa fa-handshake-o"></i></div>
            <h3>60+ Partners & Over 50 Products</h3>
            <p>Lorem Ipsum Dolor Text</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="facts-box z-index">
            <div class="icon-box"><i class="fa fa-smile-o"></i></div>
            <h3>1,00,000+ Happy Customers</h3>
            <p>Lorem Ipsum Dolor Text</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="facts-box z-index">
            <div class="icon-box"><i class="fa fa-inr"></i></div>
            <h3>Rs. 300 Cr+ Monthly Disbursement</h3>
            <p>Lorem Ipsum Dolor Text</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="facts-box z-index">
            <div class="icon-box"><i class="fa fa-shield"></i></div>
            <h3>Secure Systems</h3>
            <p>Lorem Ipsum Dolor Text</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="facts-box z-index">
            <div class="icon-box"><i class="fa fa-lock"></i></div>
            <h3>Privacy Protection</h3>
            <p>Lorem Ipsum Dolor Text</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="facts-box z-index">
            <div class="icon-box"><i class="fa fa-exchange"></i></div>
            <h3>Simple Process</h3>
            <p>Lorem Ipsum Dolor Text</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Facts -->