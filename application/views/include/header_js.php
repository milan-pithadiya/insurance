
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?=SITE_TITLE?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/png" href="<?= base_url()?>assets/img/favicon.png">
	<link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/flaticon.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/slick.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/meanmenu.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/magnific-popup.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/odometer.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/nice-select.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/responsive.css">
    <link href="<?= base_url()?>assets/css/chosen.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url()?>assets/css/developer.css">

	<link rel="stylesheet" href="<?= base_url()?>assets/authority/css/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
	<script>
		var BASE_URL = '<?= base_url();?>';
	</script>
	    