<section class="tg-haslayout">
  <div class="container">
    <div class="row">
      <div class="tg-allstatus">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <?php $statistics = get_statistics();


          $books= $statistics['books_we_have'];
          $books_we_have = number_format($books);

          $total_members = $statistics['total_members'];
          $total_members = number_format($total_members);

          $happy_users = $statistics['happy_users'];
          $happy_users = number_format($happy_users);

          ?>
          <div class="tg-parallax tg-bgbookwehave" data-z-index="2" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?= base_url()?>assets/images/parallax/bgparallax-01.jpg">
            <div class="tg-status">
              <div class="tg-statuscontent">
                <span class="tg-statusicon"><i class="icon-book"></i></span>
                <h2>Books we have<span><?=$total_members?></span></h2>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <div class="tg-parallax tg-bgtotalmembers" data-z-index="2" data-appear-bottom-offset="600" data-parallax="scroll" data-image-src="<?= base_url()?>assets/images/parallax/bgparallax-02.jpg">
            <div class="tg-status">
              <div class="tg-statuscontent">
                <span class="tg-statusicon"><i class="icon-user"></i></span>
                <h2>Total members<span><?=$total_members?></span></h2>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <div class="tg-parallax tg-bghappyusers" data-z-index="2" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?= base_url()?>assets/images/parallax/bgparallax-03.jpg">
            <div class="tg-status">
              <div class="tg-statuscontent">
                <span class="tg-statusicon"><i class="icon-heart"></i></span>
                <h2>Happy users<span><?=$happy_users?></span></h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>