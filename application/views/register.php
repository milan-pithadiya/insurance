<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body class="tg-home tg-homeone">
    <!--Wrapper Start-->
    <?php /*
    <!-- <div id="tg-wrapper" class="tg-wrapper tg-haslayout">       
        <main id="tg-main" class="tg-main tg-haslayout">
            <div class="tg-sectionspace tg-haslayout">
              <div class="container">
                <h2 align="center">Register</h2>
                <div class="contact-form">
                  <form id="form" class="tg-formtheme tg-formcontactus" method="post" enctype="multipart/form-data" action="">
                    <?php $this->load->view('authority/common/messages');?>
                    <fieldset>
                      <div class="form-group">
                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Firstname*" value="<?=(isset($first_name)?$first_name:'')?>">
                        <?= form_error("first_name", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Lastame*" value="<?=(isset($last_name)?$last_name:'')?>">
                        <?= form_error("last_name", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email Address*" value="<?=(isset($email)?$email:'')?>">
                            <?= form_error("email", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Mobile number*" value="<?=(isset($phone_number)?$phone_number:'')?>">
                            <?= form_error("phone_number", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password*">
                        <?= form_error("password", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm password*" >
                      </div>
                      <div class="form-group tg-hastextarea">
                        <textarea class="form-control" name="address" id="address" rows="1" placeholder="Address*"><?=(isset($address)?$address:'')?></textarea >
                            <?= form_error("address", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
        </main>
    </div> -->
    */ ?>
    <section class="hero-section full-screen gray-light-bg">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-md-7 col-lg-6 col-xl-8 d-none d-lg-block">
                    <div class="bg-cover vh-100 ml-n3 gradient-overlay" style="background: url(<?=base_url('assets/img/achievements-img2.jpg')?>);">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 col-xl-4">
                    <div class="login-signup-wrap px-4 my-5">
                        <h1>Become An Agent</h1>
                        <form class="login-signup-form"  id="form" method="post" enctype="multipart/form-data" action="">
                            <div class="row">
                              <?php $this->load->view('include/messages');?>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="agency_name" id="agency_name" class="form-control" required data-error="Enter Your Agency Name" placeholder="Agenacy Name" value="<?=(isset($agency_name)?$agency_name:'')?>">
                                        <?= form_error("agency_name", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control" required data-error="Please enter your email" placeholder="Email" value="<?=(isset($email)?$email:'')?>">
                                        <?= form_error("email", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" id="phone_number" name="phone_number" class="form-control" required data-error="Enter Your Contact Number" placeholder="Contact Number" value="<?=(isset($phone_number)?$phone_number:'')?>">
                                        <?= form_error("phone_number", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea type="text" name="address" id="address" class="form-control" required data-error="Enter Your Address" placeholder="Address" rows="3"><?=(isset($address)?$address:'')?></textarea>
                                        <?= form_error("address", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="zip_code" id="zip_code" class="form-control" placeholder="Zip Code" value="<?=(isset($zip_code)?$zip_code:'')?>">
                                        
                                        <?= form_error("zip_code", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control" required data-error="Enter Your Password" placeholder="Password" >
                                        <?= form_error("password", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" required data-error="Enter Confirm Your Password" placeholder="Confirm Password" >
                                        <?= form_error("confirm_password", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="file" name="image" id="image" class="form-control profile-photo" >
                                        <?= form_error("image", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <button type="submit" class="default-btn check">Submit <span></span></button>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-lg-6 col-md-6 text-right">
                                    <a href="<?=base_url('login')?>" class="default-btn">Sign in <span></span></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('include/footer_js');?>
<script>
    /*FORM VALIDATION*/
    $("#form").validate({
        rules: {
            'agency_name': {required: true}, 
            // 'last_name': {required: true}, 
            'email': {required: true,email:true}, 
            'phone_number': {required: true,minlength:10}, 
            'address': {required: true},
            'zip_code': {required: true},
            'password': {required: true}, 
            'confirm_password': {required: true,equalTo: "#password"}, 
        },
        messages: {
            'agency_name': {required:"Please enter agency name"}, 
            // 'last_name': {required:"Please enter last name"}, 
            'email': {required:"Please enter email",email:"Please enter valid email"}, 
            'phone_number': {required:"Please enter mobile number",minlength:"Enter minimum 10 digits mobile number"},
            'address': {required:"Please type your address"},
            'zip_code': {required:"Please type your zip code"},
            'password': "Please enter password",     
            'confirm_password': {required: 'Enter confirm password',equalTo: "Password and confirm password must be same"}, 
        }
    }); 
    var form = $( "#form" );
        form.validate();

        $(document).on('click','.check',function(e){
          if(!form.valid()){
            $("#form").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });     
</script>
</body>
</html>