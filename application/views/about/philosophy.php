<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>
<section id="banner">
    <div class="container-fluid">
        <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                    if (isset($slider_details) && $slider_details !=null) {
                        foreach ($slider_details as $key => $value) {
                        ?>
                            <li data-target="#myCarousel" data-slide-to="<?= $key;?>" class="<?= $key==0 ? 'active' : '';?>"></li>
                        <?php } 
                    }
                ?>
            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">
                <?php
                    if (isset($slider_details) && $slider_details !=null) {
                        foreach ($slider_details as $key => $value) {
                        ?>  
                            <div class="item <?= $key==0 ? 'active' : '';?>">
                                <img src="<?= base_url(ABOUT_US_SLIDER.$value['image']);?>" alt="Chania">
                                <div class="carousel-caption">
                                    <h3><?= $value['title'.get_language()];?></h3>
                                </div>
                            </div>
                        <?php } 
                    }
                ?>
            </div>
            <!-- /.carousel-inner -->
            <!-- Controls -->
            <div class="control-box">
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="control-icon prev fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="control-icon next fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- /.control-box -->
        </div>
    </div>
</section>
</div>
<div class="clearfix"></div>
<div class="tr1">
    <div class="col-sm-12">
        <?php
            if (isset($details) && $details !=null) {
            ?>
                <div class="side-img-detail">
                    <img src="<?= base_url(ABOUT_IMAGE.$details[0]['image']);?>" />
                </div>
                <div class="treat-inr side-img-detail-inner">
                    <?php
                        if (isset($details) && $details !=null) {
                        ?>
                            <h3><?= $details[0]['title'.get_language()];?></h3>
                            <?= $details[0]['description'.get_language()];?>
                        <?php } 
                    ?>
                </div>
            <?php } 
        ?>
    </div>
</div>
<div class="clearfix"></div>

<?php $this->load->view('include/copyright');?>
<?php $this->load->view('include/footer');?>