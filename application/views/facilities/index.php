<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>
<section id="banner">
    <div class="container-fluid">
        <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                    if (isset($slider_details) && $slider_details !=null) {
                        foreach ($slider_details as $key => $value) {
                        ?>
                            <li data-target="#myCarousel" data-slide-to="<?= $key;?>" class="<?= $key==0 ? 'active' : '';?>"></li>
                        <?php } 
                    }
                ?>
            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">
                <?php
                    if (isset($slider_details) && $slider_details !=null) {
                        foreach ($slider_details as $key => $value) {
                        ?>  
                            <div class="item <?= $key==0 ? 'active' : '';?>">
                                <img src="<?= base_url(FACILITIES_SLIDER.$value['image']);?>" alt="Chania">
                                <div class="carousel-caption">
                                    <h3><?= $value['title'.get_language()];?></h3>
                                </div>
                            </div>
                        <?php } 
                    }
                ?>
            </div>
            <!-- /.carousel-inner -->
            <!-- Controls -->
            <div class="control-box">
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="control-icon prev fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="control-icon next fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- /.control-box -->
        </div>
    </div>
</section>
</div>
<div class="clearfix"></div>
<div class="container">
    <div class="row">
        <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="gallery-title" align="center" style="text-transform: uppercase;"><?= $this->lang->line('facilities');?></h1>
        </div>
        <div class="tz-gallery gallery" id="gallery">
        	 <?php
                if (isset($details) && $details !=null) {
                    foreach ($details as $key => $value) {
                    ?>  
			            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter 3">
			                <a class="lightbox" href="<?= base_url(FACILITIES.$value['image']);?>">
			                <img src="<?= base_url(FACILITIES.$value['image']);?>" alt="Park" class="img-responsive">
			                </a>
			                <span><?= $value['title'.get_language()];?></span>
			            </div>
			        <?php } 
			    }
			?>
        </div>
        <div class="clearfix"></div>
        <br>
    </div>
</div>
<div class="clearfix"></div>
<?php $this->load->view('include/copyright');?>
<?php $this->load->view('include/footer');?>