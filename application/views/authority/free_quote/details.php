<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Quote</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <?php
                                                            if(isset($name) && $name !=null){ ?>
                                                                <tr>
                                                                    <td><b>Name:</b></td>
                                                                    <td><?= $name?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                       
                                                        <?php
                                                            if(isset($email) && $email !=null){ ?>
                                                                <tr>
                                                                    <td><b>Email Address:</b></td>
                                                                    <td><?=$email?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($mobile_number) && $mobile_number !=null){ ?>
                                                                <tr>
                                                                    <td><b>Mobile Number:</b></td>
                                                                    <td><?=$mobile_number?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($created_at) && $created_at !=null){ ?>
                                                                <tr>
                                                                    <td><b>Contact date:</b></td>
                                                                    <td><?= format_date_Mdy_time($created_at)?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>