<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<style type="text/css">
    .links tr td:nth-child(1){
        font-weight: bold;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Social-links</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        	<?php $this->load->view('authority/common/messages')?>
        	
            <div class="row">
	        	<div class="col-md-6">
	        		
	        	</div>
                <div class="col-md-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Social Links</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="data-response">
                                	<?php
										if (isset($details) && $details !=null){
		                                    foreach ($details  as $key => $value) {
		                                    	$id = $value['id'];
											?>
			                                    <tr>
							                        
							                        <td>
                                                        <table class="links table table-responsive">
                                                            <tr>
                                                                <td>Facebook-link</td>
                                                                <td><?= $value['facebook_link'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Instagram-link</td>
                                                                <td><?= $value['instagram_link'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Twiter-link</td>
                                                                <td><?= $value['twiter_link'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Google+</td>
                                                                <td><?= $value['google_plus_link'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Linked-in</td>
                                                                <td><?= $value['link_in_link'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Skype-link</td>
                                                                <td><?= $value['skype_link'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Pinterest-link</td>
                                                                <td><?= $value['pinterest_link'];?></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Youtube-link</td>
                                                                <td><?= $value['youtube_link'];?></td>
                                                            </tr>
                                                        </table>
							                        </td>
							                        <td>
										                <a href="<?= base_url('authority/social-links/edit/'.$id);?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>
							                        </td>
							                    </tr>
			                                <?php } 
			                            }
			                            else{
			                            	?>
			                            		<tr data-expanded="true">
													<td colspan="7" align="center">Records not found</td>
												</tr>
			                            	<?php
			                            }
			                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<?php $this->view('authority/common/footer'); ?>									