<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Life & Health sub of sub menu</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                
                <!-- /.col -->
                <div class="col-md-12">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    // $action = ($details == null) ? base_url('authority/home_slider/add_slider') : '';                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open("", $attributes);
                                ?>
                                    <input type="hidden" name="id" id="submenu_id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">
                                    <div class="form-group row">
                                        <label for="submenu_id" class="col-sm-3 col-form-label">Sub Menu <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="submenu_id" name="submenu_id">
                                                <option>--Sub Menu--</option>
                                                <?php 
                                                    $life_and_health = life_and_health_sub();
                                                    if (isset($life_and_health) && $life_and_health != null) {
                                                        foreach ($life_and_health as $key => $value) { ?>
                                                            <option value="<?=$value['id']?>" <?=(isset($details[0]['submenu_id']) && $value['id']==$details[0]['submenu_id'])?'selected':''?>><?=$value['title']?></option>
                                                            <?php        
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Title<span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="title" placeholder="Enter title" class="form-control" value="<?= isset($details[0]['title']) && $details[0]['title'] !=null ? $details[0]['title'] : '';?>">
                                            <?= form_error("title", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                title: {required:true,maxlength:100},
                submenu_id: {required:true},

            },
            messages: {     
                title: {
                    required :"Please enter title",
                    maxlength:"Allowd only 100 character"
                },
                submenu_id: {
                    required :"Please select main menu",
                },
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>