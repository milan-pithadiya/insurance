<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Models</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                
                <!-- /.col -->
                <div class="col-md-12">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    // $action = ($details == null) ? base_url('authority/home_slider/add_slider') : '';                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open("", $attributes);
                                ?>
                                    <input type="hidden" name="id" id="models_id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">
                                    <div class="form-group row">
                                        <div class="col-md-6 row">
                                            <label for="year_id" class="col-sm-12 col-form-label">Select Year <span class="required">*</span></label>
                                            <div class="col-sm-12">
                                                <select class="form-control" id="year_id" name="year_id">
                                                    <option>--Select Year--</option>
                                                    <?php 
                                                        $year = year();
                                                        if (isset($year) && $year != null) {
                                                            foreach ($year as $key => $value) { ?>
                                                                <option value="<?=$value['id']?>" <?=(isset($details[0]['year_id']) && $value['id']==$details[0]['year_id'])?'selected':''?>><?=$value['title']?></option>
                                                                <?php        
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 row">
                                            <label for="make_id" class="col-sm-12 col-form-label">Select Make <span class="required">*</span></label>
                                            <div class="col-sm-12">
                                                
                                                <?php

                                                        if (isset($details[0]['year_id']) && $details[0]['year_id'] != '') {
                                                            $make_info = make(array('year_id'=>$details[0]['year_id']));
                                                            //echo "<pre>";print_r($make_info);exit;
                                                            $options = array();
                                                            $options[NULL] = '-- Select Make --';
                                                            if (count($make_info) > 0) {
                                                                foreach ($make_info as $key1 => $value1) {
                                                                    $options[$value1['id']] = $value1['title'];
                                                                }
                                                            }
                                                        } else {
                                                            $options = array(
                                                                NULL => 'Select Make',
                                                            );
                                                        }
                                                        echo form_dropdown('make_id', $options, isset($details[0]['make_id']) ? $details[0]['make_id'] : '', 'class="form-control" id="make_id"');
                                                    ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-12 col-form-label">Title<span class="required">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" name="title" placeholder="Enter title" class="form-control" value="<?= isset($details[0]['title']) && $details[0]['title'] !=null ? $details[0]['title'] : '';?>">
                                            <?= form_error("title", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                title: {required:true,maxlength:100},
                make_id: {required:true},

            },
            messages: {     
                title: {
                    required :"Please enter title",
                    maxlength:"Allowd only 100 character"
                },
                make_id: {
                    required :"Please select main menu",
                },
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>