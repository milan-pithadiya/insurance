<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">General Contact</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <?php
                                                            if(isset($name) && $name !=null){ ?>
                                                                <tr>
                                                                    <td><b>Name:</b></td>
                                                                    <td><?= $name?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($selected_item) && $selected_item !=null){ ?>
                                                                <tr>
                                                                    <td><b>Selected:</b></td>
                                                                    <td><?=$selected_item?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($email) && $email !=null){ ?>
                                                                <tr>
                                                                    <td><b>Email Address:</b></td>
                                                                    <td><?=$email?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($phone_number) && $phone_number !=null){ ?>
                                                                <tr>
                                                                    <td><b>Mobile Number:</b></td>
                                                                    <td><?=$phone_number?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($address) && $address !=null){ ?>
                                                                <tr>
                                                                    <td><b>Address:</b></td>
                                                                    <td><?= isset($address) && $address !=null ? $address : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($preferred_language) && $preferred_language !=null){
                                                                $preferred_language = get_language_titles($preferred_language);               
                                                                ?>
                                                                <tr>
                                                                    <td><b>Preferred language:</b></td>
                                                                    <td><?= $preferred_language?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        
                                                        <?php
                                                            if(isset($business_name) && $business_name !=null){ ?>
                                                                <tr>
                                                                    <td><b>Business name:</b></td>
                                                                    <td><?= $business_name?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($business_address) && $business_address !=null){ ?>
                                                                <tr>
                                                                    <td><b>Business address:</b></td>
                                                                    <td><?= $business_address?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        
                                                        <?php
                                                            if(isset($category_id) && $category_id !=null){
                                                                             
                                                                ?>
                                                                <tr>
                                                                    <td><b>Category :</b></td>
                                                                    <td><?= get_category_title($category_id)?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($product_id) && $product_id !=null){
                                                                             
                                                                ?>
                                                                <tr>
                                                                    <td><b>Product :</b></td>
                                                                    <td><?= get_product_title($product_id)?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($created_at) && $created_at !=null){ ?>
                                                                <tr>
                                                                    <td><b>Contact date:</b></td>
                                                                    <td><?= format_date_Mdy_time($created_at)?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($message) && $message !=null){ ?>
                                                                <tr>
                                                                    <td><b>Message:</b></td>
                                                                    <td><?= $message?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>