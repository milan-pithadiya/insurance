<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Books</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <?php
                                
                                if (isset($details) && $details !=null) {
                                ?>
                                    <div class="text-center">
                                        <label>Current Image</label><br>
                                        <img class="profile-user-img img-fluid img-circle" src="<?= base_url(BOOK_IMAGE).$details[0]['image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" alt="profile picture">
                                    </div>
                                <?php } 
                            ?>
                        </div>
                    </div>
                    <?php
                        if (isset($details) &&  $details !=null) { ?>
                            <div class="card card-primary card-outline">
                                <div class="card-body box-profile">
                                    <div class="text-center">
                                        <a href="<?= base_url(BOOK_PDF).$details[0]['book_pdf']?>" target="_blank">
                                            <label>Open E-Book</label>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php 
                        } 
                    ?>
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $action = ($details == null) ? base_url('authority/books/add_book') : '';
                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open($action, $attributes);
                                ?>
                                    <input type="hidden" name="id" id="book_id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Title <span class="required">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="title" placeholder="Enter title" class="form-control" value="<?= isset($details[0]['title']) && $details[0]['title'] !=null ? $details[0]['title'] : '';?>">
                                            <?= form_error("title", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Category </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="category" placeholder="Enter category" class="form-control" value="<?= isset($details[0]['category']) && $details[0]['category'] !=null ? $details[0]['category'] : '';?>">
                                            <?= form_error("category", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Author <span class="required">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="author_name" placeholder="Enter author name" class="form-control" value="<?= isset($details[0]['author_name']) && $details[0]['author_name'] !=null ? $details[0]['author_name'] : '';?>">
                                            <?= form_error("author_name", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    
                                    <!-- <div class="form-group row">
                                        <label for="inputName2" class="col-sm-2 col-form-label">Position</label>
                                        <div class="col-sm-10">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'text',
                                                    'class' => 'form-control only_digits',
                                                    'name' => 'position',
                                                    'maxlength' => '3',
                                                    'placeholder' => 'Enter position',
                                                    'value' => (isset($details[0]['position']) && $details[0]['position'] !=null ? $details[0]['position'] : ''),
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div> -->

                                    <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-2 col-form-label">Image:<span class="required">*<br>(800 x 1130px)</span></label>
                                        <div class="col-sm-10">
                                            <input type="file" name="image" class="form-control" accept="image/*">
                                            <span id="image_error"></span>
                                            <?= form_error("image", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-2 col-form-label">PDF File:<span class="required">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="file" name="book_pdf" class="form-control" accept=".pdf">
                                            <span id="book_pdf_error"></span>
                                            <?= form_error("book_pdf", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        var book_id = $("#book_id").val();
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                title: {required:true,maxlength:100},              
                // category: {required:true,maxlength:100},              
                author_name: {required:true},              
                image: { 
                    required: function(element) {
                        if (book_id == '') {  
                            return true;
                        }
                        else {
                            return false;
                        }
                    }, 
                },
                book_pdf: { 
                    required: function(element) {
                        if (book_id == '') {  
                            return true;
                        }
                        else {
                            return false;
                        }
                    }, 
                },                  
            },
            messages: {     
                title: {required :"Please enter title",maxlength:"Allowd only 100 character"},   
                // category: {required :"Please enter category"},   
                author_name: {required :"Please enter author name"},   
                image: {required :"Please upload image"},       
                book_pdf: {required :"Please upload pdf file"},       
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>