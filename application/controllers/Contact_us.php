<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {
	public function index()
	{
		$data = array();
		if($this->input->post())
		{
			$data = $this->input->post();		
			// echo "<pre>";print_r($data);exit();
			$this->form_validation->set_rules('name', 'name', 'required',array('required' => 'Please type your name'));
	        
	        $this->form_validation->set_rules('mobile_number', 'mobile number', 'required|min_length[10]|max_length[15]', array('required' => 'Please enter mobile number'));
	        $this->form_validation->set_rules('email', 'email address', 'required|valid_email', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address"));
			$this->form_validation->set_rules('message', 'message', 'required',array('required' => 'Please type your message'));
	        if ($this->form_validation->run() == FALSE)
	        {
	            $this->load->view('contact_us',$data);
	        }else{
				$record = $this->production_model->insert_record('contact_us',$data);
				if ($record !='') {
					// $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',array($data['email']),'','mail_form/thankyou_page/contact_us','',''); // user send email thank-you page
					// $admin_email = $this->production_model->get_all_with_where('administrator','','',array()); 
					// $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/contact_us',$data,''); 
					// admin send mail
					$this->session->set_flashdata('success', 'Our executive will contact you soon.');
					redirect($_SERVER['HTTP_REFERER']);
				}else{
					$this->session->set_flashdata('error', 'Please try later...!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
		}
		$this->load->view('contact_us',$data);
	}
	
}
?>