<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_quote_form_auto extends CI_Controller {
	public function index($selected_item="")
	{
		$data = array();
		$data['selected_item']= (isset($selected_item))?ucwords(str_replace('-', ' ', $selected_item)):'';
		if($this->input->post())
		{
			$data = $this->input->post();
			$langs = $this->input->post('preferred_language');
			if(!empty($langs)){

				$langs = implode(",", $langs);
				$data['preferred_language'] =$langs;
			}
				
	        $this->form_validation->set_rules('preferred_language[]', 'preferred_language name', 'required', array('required' => 'Please select any one language'));
	        // $this->form_validation->set_rules('last_name', 'last name', 'required', array('required' => 'Please enter email last name'));
	        // $this->form_validation->set_rules('email', 'email address', 'required|valid_email', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address"));
	        
	        // $this->form_validation->set_rules('mobile_number', 'mobile number', 'required|min_length[10]|max_length[15]', array('required' => 'Please enter mobile number'));
	        if ($this->form_validation->run() == FALSE)
	        {
	            $this->load->view('general_quote_form_auto',$data);
	        }else{
	        	echo $count_year_record = count($this->input->post('year_id'));
	        	$year_ids = $this->input->post('year_id');
	        	$make_ids = $this->input->post('make_id');
	        	$models_ids = $this->input->post('models_id');
	        	$vins = $this->input->post('vin');
	        	unset($data['year_id']);
	        	unset($data['make_id']);
	        	unset($data['models_id']);
	        	unset($data['vin']);
	        	unset($data['quote_form_id']);
	        	// echo "<pre>";print_r($data);exit;
				$record = $this->production_model->insert_record('general_quote_auto',$data);
				// $record = 1;

				if ($record !='') {

					$vehical_data['quote_form_id']  = $record;
					for ($i=0; $i <$count_year_record ; $i++) { 
						$vehical_data['year_id']=$year_ids[$i];
						$vehical_data['make_id']=$make_ids[$i];
						$vehical_data['models_id']=$models_ids[$i];
						$vehical_data['vin']=$vins[$i];
						$vehical_record = $this->production_model->insert_record('vehical',$vehical_data);
					}
					// print_r($vehical_data);exit;

					// $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',array($data['email']),'','mail_form/thankyou_page/contact_us','',''); // user send email thank-you page
					// $admin_email = $this->production_model->get_all_with_where('administrator','','',array()); 
					// $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/contact_us',$data,''); 
					// admin send mail
					// $this->session->set_flashdata('success', 'Thank you !!!');
					$this->session->set_userdata('quote_form_id', $record);
					redirect('general-quote-form-auto/second');

				}else{
					$this->session->set_flashdata('error', 'Please try later...!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
		}
		$this->load->view('general_quote_form_auto',$data);
	}
	public function second()
	{
		if(!$this->session->userdata('quote_form_id')){
			redirect(base_url(),'refresh');
		}
		$data = array();
		if($this->input->post())
		{
			$data = $this->input->post();
	        $this->form_validation->set_rules('name', 'name', 'required', array('required' => 'Please enter email last name'));
	        $this->form_validation->set_rules('email', 'email address', 'required|valid_email', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address"));
	        if ($this->form_validation->run() == FALSE)
	        {
	            $this->load->view('general_quote_form_auto_second',$data);
	        }else{
	        	
	        	// echo "<pre>";print_r($data);exit;
	        	if ($_FILES['image']['name'] !='') {
	        		$data['image'] = $this->production_model->other_file_upload(GENERAL_QUOTE_DOCS,'image');
	        	}
	        	$where['id']=$this->session->userdata('quote_form_id');
	        	$record = $this->production_model->update_record('general_quote_auto',$data,$where);

				if ($record !='') {
					$this->session->unset_userdata('quote_form_id');
					// print_r($vehical_data);exit;

					// $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',array($data['email']),'','mail_form/thankyou_page/contact_us','',''); // user send email thank-you page
					// $admin_email = $this->production_model->get_all_with_where('administrator','','',array()); 
					// $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/contact_us',$data,''); 
					// admin send mail
					// $this->session->set_userdata('quote_form_id', $record);
					$this->session->set_flashdata('success', 'Thank you !!!');
					?> 
					<script type="text/javascript">
						setTimeout(function() {
							<?php redirect(base_url()); ?>		
						}, 3000);
					</script>
					<?php
					
					
				}else{
					$this->session->set_flashdata('error', 'Please try later...!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
		}
		$this->load->view('general_quote_form_auto_second',$data);
	}
	function get_make_list()
    {
        if ($this->input->post('year_id')) {
            $year_id = $this->input->post('year_id');    
        }
        $where['year_id']=$year_id;
        $record = $this->production_model->get_all_with_where('make','','',$where);
        if ($record > 0) {
        	if (isset($record) && $record != null) {?>
        		
        		<span class="current"><?=$record[0]['title']?></span>
        		<ul class="list">
	        		<?php
	        			$i=0;
		                foreach ($record as $key => $value) { ?>
		                	<option value="<?=$value['id']?>" style="display: none"><?=$value['title']?></option>
		                		<li data-value="<?=$value['id']?>" class="option <?=($i==0)?'selected focus':''?>>"><?=$value['title']?></li>
		                    <?php   $i++;     
	                	}
	                ?>
                </ul>
                <?php
            }
        }else{
        	?>
        	<option>-- Select Make --</option>
        	<?php
        }
    }
    function get_models_list()
    {
    	// echo "<pre>";print_r($this->input->post());exit;
        if ($this->input->post('make_id')) {
            $make_id = $this->input->post('make_id'); 
            $where['make_id']=$make_id;
        $record = $this->production_model->get_all_with_where('models','','',$where);
        if ($record > 0) {
        	if (isset($record) && $record != null) {?>
        		<span class="current"><?=$record[0]['title']?></span>
        		<ul class="list">
	        		<?php
	        			$i=0;
		                foreach ($record as $key => $value) { ?>
		                		<option value="<?=$value['id']?>" style="display: none"><?=$value['title']?></option>
		                		<li data-value="<?=$value['id']?>" class="option <?=($i==0)?'selected focus':''?>>"><?=$value['title']?></li>
		                    <?php   $i++;     
	                	}
	                ?>
                </ul>
                <?php
            }
        }else{
        	?>
        	<option>-- Select Model --</option>
        	<?php
        }   
        }
        
    }	
}
?>