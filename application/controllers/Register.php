<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Register extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
		}
		
		public function index()
		{
			$data= array();
			if($this->input->post())
			{
				$data = $this->input->post();
				
				$this->form_validation->set_rules('agency_name', 'agency_name', 'required');
				$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[user.email]');
				$this->form_validation->set_rules('phone_number', 'phone_number', 'required|is_unique[user.phone_number]');
				$this->form_validation->set_rules('password', 'password', 'required');
				$this->form_validation->set_rules('confirm_password', 'confirm_password', 'required|matches[password]');
				$this->form_validation->set_rules('zip_code', 'zip_code', 'required');

				if ($this->form_validation->run() == FALSE)
		        {		        	
		            $data = array_merge($data, $_POST);
		        }
		        else
		        {
		        	$data['email'] = strtolower($data['email']);
		        	$data['created_at'] = getCurrentDateWithTime();
		        	unset($data['confirm_password']);
		        	
		        	$data['password'] = $this->encryption->encrypt($data['password']);
		        	$data['otp'] = generate_otp(6);

		          	$get_email = $this->production_model->get_all_with_where('user','','',array('email'=>$data['email']));
		          	$get_user_mobile = $this->production_model->get_all_with_where('user','','',array('phone_number'=>$data['phone_number']));
		          	// echo "<pre>";print_r($get_email);exit;
					if (count($get_email) > 0) {
						$this->session->set_flashdata('error', 'Email allredy exists....!');
						$data = array_merge($data, $_POST);

					}else if (count($get_user_mobile) > 0) {
						$this->session->set_flashdata('error', 'Phone number allredy exists....!');
						$data = array_merge($data, $_POST);
					}
					else
					{	
						if ($_FILES['image']['name'] !='') {
	                        $data['image'] = $this->production_model->image_upload(PROFILE_PICTURE,'image');
	                    }		
						$record = $this->production_model->insert_record('user',$data);
						if ($record !='') {
							$data['user_id'] = $record;
							// $send_mail = $this->production_model->mail_send(SITE_TITLE,array($data['email']),'','mail_form/thankyou_page/registration',$data,''); 

							// $admin_email = $this->production_model->get_all_with_where('administrator','','',array()); 
							// $admin_send_mail = $this->production_model->mail_send(SITE_TITLE,array(ADMIN_EMAIL),'','mail_form/admin_send_mail/registration',$data,''); // admin send mail
							// $this->session->set_flashdata('success', 'Thank you for registration please check your mail');
							$this->session->set_flashdata('success', 'Thank you for registration');
							// redirect('register');
							redirect('login');
						}
						else
						{
							$this->session->set_flashdata('error', 'Not Registered....!');
							// redirect($_SERVER['HTTP_REFERER']);
						}
					}	
				}	
			}
			$this->load->view('register',$data);
		}		
		public function verify($user_id){ 
			if($user_id != ""){
				$data['user_id']=$user_id;
				
				if($this->input->post()){
					$user_chk = $this->production_model->get_all_with_where('user','','',array('id'=>$this->input->post('user_id'),'otp'=>$this->input->post('otp')));
					// echo "<pre>";echo $this->db->last_query();print_r($user_chk);exit;
					if(!empty($user_chk)){
						$records = array(
							'status' => '1',
							'modified_at' => getCurrentDateWithTime()
						);
						$conditions = array(
							"where" => array("id" => $this->input->post('user_id')),
						);
						$this->common_model->update_data('user', $records, $conditions);
						//redirect('login');
						$session = array(
							'login_id' => $user_chk[0]['user_id'],
							'username' => $user_chk[0]['agency_name'],
							'useremail' => $user_chk[0]['email'],
							'useremobile' => $user_chk[0]['phone_number'],
						);		
						$this->session->set_userdata($session);
						redirect(base_url('login'));
					}else{
						$this->session->set_flashdata('error','You have entered wrong OTP ...!');
						redirect(base_url('register/verify/'.$user_id));
					}
				}
				$this->load->view('verify_otp',$data);
			}
		}
	}
?>