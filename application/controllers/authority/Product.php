<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index() {
        $data = array();  
        $tmp_data = product();
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/product/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        $data['details'] = $this->production_model->get_all_with_where_limit('product','id','asc',array(),$record['limit'],$record['start']); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        $this->load->view('authority/product/view',$data);
    }
    
    function add_edit($id='')
    {   
        $data = $this->input->post();

        $data['details'] = array();
        if($id != ""){
            $condition = array('id'=>$id);
            $data['details'] = product($condition);
        }

        if($this->input->post()){
            $this->validate($id);
            if ($this->form_validation->run() == FALSE)
            {
                $data['details'] = array();
                $this->load->view('authority/product/add-edit',$data);
            }
            else
            {
                if($id == ""){
                    unset($data['details']);                  
                    $record = $this->production_model->insert_record('product',$data);
                    if ($record !='') {
                        $this->session->set_flashdata('success', 'Added successful.');
                        redirect(base_url('authority/product')); 
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'Not added.');
                        redirect($_SERVER['HTTP_REFERER']);
                    }   
                }else if($id != ""){
                    unset($data['details']);

                    $record = $this->production_model->update_record('product',$data,array('id'=>$id));
                    if($record == 1) {
                        $this->session->set_flashdata('success', 'Updated successful.');
                        redirect(base_url('authority/product'));
                    }else{
                        $this->session->set_flashdata('error', 'Not updated.');
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                }
            }
        }
        $this->load->view('authority/product/add-edit',$data);
    }

    function delete()
    {
        if ($this->input->post('id')) {
            $id = $this->input->post('id');    
        }else if ($this->input->post('chk_multi_checkbox')) {
            $id = $this->input->post('chk_multi_checkbox');   
        }
        $record = $this->production_model->get_delete_where_in('product','id',$id);
        if ($record == 1) {
            $response_array['success'] = true;
            $response_array['message'] = 'Deleted successful.';
        }
        echo json_encode($response_array); exit;
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');

        $record = $this->production_model->get_delete_where_in('product','id',$chkbox_id);
        if ($record != 0) {
            $response_array['success'] = true;
            $response_array['message'] = 'Deleted successful.';
        }
        echo json_encode($response_array); exit;
    } 
    function filter()
    {
        $this->session->product_info = $_POST;
        $name = isset($this->session->product_info['name']) ? $this->session->product_info['name'] : '';
        if (isset($name) && $name !=null) {
            $this->db->group_start();
            $this->db->like('title', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post(); 
        $tmp_data = product();
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/product/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        $or_like_array =array();

        if (isset($name) && $name !=null) {
            
            $or_like_array['product.title']=$name;
            $or_like_array['category.title']= $name;
        }

        // $filteredData = $this->production_model->get_all_with_where_limit('product','id','desc',array(),$record['limit'],$record['start']);

        $join[0]['table_name'] = 'category';
        $join[0]['column_name'] = 'category.id = product.category_id';
        $join[0]['type'] = 'left';


        $filteredData = $this->production_model->jointable_descending(array('product.*','category.title as main_title'),'product','',$join,'product.id','desc',array(),array(),array(),$record['limit'],$record['start'],$or_like_array);
        // echo"<pre>".$this->db->last_query(); print_r($filteredData); exit;
        // $filteredData = $this->production_model->get_all_with_where_limit('product','id','desc',array(),$record['limit'],$record['start']); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        ob_start();
        if (isset($filteredData) && !empty($filteredData) ) { 
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                    <tr>
                        <td style="width: 10px;">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id;?>" value="<?= $id?>">
                                <label for="customCheckbox<?= $id;?>" class="custom-control-label"></label>
                            </div>
                        </td>
                        <td><?= $key+$record['no'];?></td>
                        <td><?= get_category_title($value['category_id']);?>
                        <td><?= $value['title'];?></td>
                        <td>
                            <a href="<?= base_url('authority/product/add-edit/'.$id);?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>
                            
                            <a href="javascript:void(0)" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id;?>"><i class="fa fa-trash-o"></i></a>

                            <?php 
                                if($value['status'] == '1'){
                                    echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status" data-table="product" data-id="'.$id.'" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
                                    } else {
                                    echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status" data-table="product" data-id="'.$id.'" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
                                } 
                            ?>
                        </td>
                    </tr>
                <?php
            }   
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();                
            $response_array['pagination'] = $data['pagination'];                
        }else{
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">Records not found</td>
                                            </tr>'; 
            $response_array['pagination'] = '';                     
        }           
        echo json_encode($response_array); exit;
    } 
    function validate(){
        $this->form_validation->set_rules('title', 'title', 'required', array('required' => 'Please enter question'));
    }
    
}
?>