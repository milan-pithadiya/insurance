<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->helper('form');
    }

    public function index() {
        $data = array();
        $data = array_merge($data, array("form_title" => "Settings"));

        $conditions = array(
            "select" => "*",
        );
        $site_settings = $this->common_model->select_data("site_settings", $conditions);
        $data = array_merge($data, array("site_settings" => $site_settings));

        $this->load->library("form_validation");

        if ($this->input->method() == "post") {
            if (isset($site_settings['row_count']) && $site_settings['row_count'] > 0) {
                foreach ($site_settings['data'] as $value) {
                    if ($value['is_required'] == "1") {
                        $this->form_validation->set_rules($value['setting_key'], $value['setting_title'], 'required', array('required' => 'Please enter ' . $value['setting_title'] . ' '));
                    }
                }
            }

            if ($this->form_validation->run() === FALSE) {
                
            } else {
                if (isset($site_settings['row_count']) && $site_settings['row_count'] > 0) {
                    foreach ($site_settings['data'] as $value) {
                        $records = array(
                            'setting_value' => $this->input->post($value['setting_key']),
                        );
                        $conditions = array(
                            "where" => array("setting_key" => $value['setting_key']),
                        );
                        $this->common_model->update_data("site_settings", $records, $conditions);
                    }
                }
                $this->session->set_flashdata('success', 'Record updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }

        $this->load->view('authority/settings', $data);
    }

}
