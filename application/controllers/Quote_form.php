<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quote_form extends CI_Controller {
	public function index($type_of_business)
	{
		$data = array();
		$data['type_of_business']= (isset($type_of_business))?ucwords(str_replace('-', ' ', $type_of_business)):'';
		if($this->input->post())
		{
			$data = $this->input->post();
			$langs = $this->input->post('preferred_language');
			if(!empty($langs)){

				$langs = implode(",", $langs);
				$data['preferred_language'] =$langs;
			}
				
	        $this->form_validation->set_rules('first_name', 'first name', 'required', array('required' => 'Please enter first name'));
	        $this->form_validation->set_rules('last_name', 'last name', 'required', array('required' => 'Please enter email last name'));
	        $this->form_validation->set_rules('email', 'email address', 'required|valid_email', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address"));
	        $this->form_validation->set_rules('business_address', 'business_address', 'required', array('required' => 'Please enter business address'));
	        // $this->form_validation->set_rules('preferred_language', 'preferred_language', 'required', array('required' => 'Please select preferred language'));
	        $this->form_validation->set_rules('mobile_number', 'mobile number', 'required|min_length[10]|max_length[15]', array('required' => 'Please enter mobile number'));
	        if ($this->form_validation->run() == FALSE)
	        {
	            $this->load->view('quote_form',$data);
	        }else{
				$record = $this->production_model->insert_record('quote',$data);
				if ($record !='') {
					// $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',array($data['email']),'','mail_form/thankyou_page/contact_us','',''); // user send email thank-you page
					// $admin_email = $this->production_model->get_all_with_where('administrator','','',array()); 
					// $send_mail = $this->production_model->mail_send(SITE_TITLE.' Contact',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/contact_us',$data,''); 
					// admin send mail
					$this->session->set_flashdata('success', 'Based on the information you provided, we’d prefer to speak by phone to get you the best coverage for your needs..');
					redirect($_SERVER['HTTP_REFERER']);
				}else{
					$this->session->set_flashdata('error', 'Please try later...!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
		}
		$this->load->view('quote_form',$data);
	}
	public function free_quote()
	{
		$data = array();
			
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        if($this->input->post()){
			$data = $this->input->post();	
			// echo "<pre>";print_r($data);exit;	
			// $data['property_used_for']= (isset($type_of_business))?ucwords(str_replace('-', ' ', $data['property_used_for'])):'';
				
	        $this->form_validation->set_rules('name', 'name', 'required', array('required' => 'Please enter your name'));
	        $this->form_validation->set_rules('email', 'email address', 'required|valid_email', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address"));
	        $this->form_validation->set_rules('mobile_number', 'mobile number', 'required|min_length[10]|max_length[15]', array('required' => 'Please enter mobile number'));
	        $this->form_validation->set_rules('property_used_for', 'property_used_for', 'required', array('required' => 'Please select property used for'));

	        if ($this->form_validation->run() == FALSE)
	        {
	            foreach ($this->form_validation->error_array() as $key => $value) {		            
		            $response_array['message'] = array($key => $value);
		            echo json_encode(returnResponse('Fail',$response_array['message'],null));
		            break;
		        }
	        }else{
				$record = $this->production_model->insert_record('free_quote',$data);
				if($record){
					echo json_encode(returnResponse('Success','Free quote added successfully!!',null));
				}else{
					echo json_encode(returnResponse('Fail','Free quote not added !!',null));
				}
				
			}
		}
	
	}
	
}
?>