<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Language_model extends CI_model {
    public function __construct() {
        if ($this->session->userdata('language') == '') { 
            $session = array('language' => 'english'); // change default lang.selection in page load
            $this->session->set_userdata($session);            
        }
        define('LANGUAGE',$this->session->userdata('language'));

        if(LANGUAGE == 'english'){
           $this->lang->load('english_lang','english'); // fisrt para. of filename , foldername.
        }else if(LANGUAGE == 'hindi'){
           $this->lang->load('hindi_lang','hindi'); // fisrt para. of filename , foldername.
        }
    }    
}
?>