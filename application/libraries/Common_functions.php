<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common_functions {

    public function __construct($params = array()) {
        $this->CI = & get_instance();
    }

    public function updatesession($id) {
        $conditions = array("where" => array("id" => $id));
        $user_info = $this->CI->common_model->select_data('administrator', $conditions);
        $info = array();
        if ($user_info['row_count'] > 0) {
            $info = $user_info['data'][0];
        }
        $this->CI->session->user_info = $info;
    }

}
